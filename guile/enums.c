/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 4 -*- */

/* $Id$ */

/* Copyright (C) 1998-99 Martin Baulig
   This file is part of LibGTop 1.0.

   Contributed by Martin Baulig <martin@home-of-linux.org>, April 1998.

   LibGTop is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

   LibGTop is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
   for more details.

   You should have received a copy of the GNU General Public License
   along with LibGTop; see the file COPYING. If not, write to the
   Free Software Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.
*/

#include "guile.h"

static sgtk_enum_literal _libgtop_parameter_literals[5] = {
    { SCM_UNDEFINED, "error-method", GLIBTOP_PARAM_ERROR_METHOD },
    { SCM_UNDEFINED, "features", GLIBTOP_PARAM_FEATURES },
    { SCM_UNDEFINED, "ncpu", GLIBTOP_PARAM_NCPU },
    { SCM_UNDEFINED, "os-version-code", GLIBTOP_PARAM_OS_VERSION_CODE },
    { SCM_UNDEFINED, "required", GLIBTOP_PARAM_REQUIRED },
};

sgtk_enum_info sgtk_libgtop_parameter_type_info = {
    { "LibGTopParameter", GTK_TYPE_ENUM },
    5, _libgtop_parameter_literals,
};

static sgtk_enum_literal _libgtop_method_literals[4] = {
    { SCM_UNDEFINED, "direct", GLIBTOP_METHOD_DIRECT },
    { SCM_UNDEFINED, "pipe", GLIBTOP_METHOD_PIPE },
    { SCM_UNDEFINED, "inet", GLIBTOP_METHOD_INET },
    { SCM_UNDEFINED, "unix", GLIBTOP_METHOD_UNIX }
};

sgtk_enum_info sgtk_libgtop_method_type_info = {
    { "LibGTopMethod", GTK_TYPE_ENUM },
    4, _libgtop_method_literals,
};

static sgtk_enum_literal _libgtop_error_method_literals[4] = {
    { SCM_UNDEFINED, "ignore", GLIBTOP_ERROR_METHOD_IGNORE },
    { SCM_UNDEFINED, "warn-once", GLIBTOP_ERROR_METHOD_WARN_ONCE },
    { SCM_UNDEFINED, "warn", GLIBTOP_ERROR_METHOD_WARN },
    { SCM_UNDEFINED, "abort", GLIBTOP_ERROR_METHOD_ABORT }
};

sgtk_enum_info sgtk_libgtop_error_method_type_info = {
    { "LibGTopErrorMethod", GTK_TYPE_ENUM },
    4, _libgtop_error_method_literals,
};

static sgtk_enum_literal _libgtop_error_literals[5] = {
    { SCM_UNDEFINED, "unknown", GLIBTOP_ERROR_UNKNOWN },
    { SCM_UNDEFINED, "invalid-argument", GLIBTOP_ERROR_INVALID_ARGUMENT },
    { SCM_UNDEFINED, "no-such-parameter", GLIBTOP_ERROR_NO_SUCH_PARAMETER },
    { SCM_UNDEFINED, "readonly-value", GLIBTOP_ERROR_READONLY_VALUE },
    { SCM_UNDEFINED, "size-mismatch", GLIBTOP_ERROR_SIZE_MISMATCH }
};

sgtk_enum_info sgtk_libgtop_error_type_info = {
    { "LibGTopError", GTK_TYPE_ENUM },
    4, _libgtop_error_literals,
};

static sgtk_enum_literal _libgtop_process_selection_literals[8] = {
    { SCM_UNDEFINED, "all", GLIBTOP_KERN_PROC_ALL },
    { SCM_UNDEFINED, "pid", GLIBTOP_KERN_PROC_PID },
    { SCM_UNDEFINED, "pgrp", GLIBTOP_KERN_PROC_PGRP },
    { SCM_UNDEFINED, "session", GLIBTOP_KERN_PROC_SESSION },
    { SCM_UNDEFINED, "tty", GLIBTOP_KERN_PROC_TTY },
    { SCM_UNDEFINED, "uid", GLIBTOP_KERN_PROC_UID },
    { SCM_UNDEFINED, "ruid", GLIBTOP_KERN_PROC_RUID },
    { SCM_UNDEFINED, "ppid", GLIBTOP_KERN_PROC_PPID }
};

sgtk_enum_info sgtk_libgtop_process_selection_type_info = {
    { "LibGTopProcessSelection", GTK_TYPE_ENUM },
    8, _libgtop_process_selection_literals,
};

static sgtk_enum_literal _libgtop_process_selection_flags_literals[3] = {
    { SCM_UNDEFINED, "exclude-idle", GLIBTOP_EXCLUDE_IDLE },
    { SCM_UNDEFINED, "exclude-system", GLIBTOP_EXCLUDE_SYSTEM },
    { SCM_UNDEFINED, "exclude-notty", GLIBTOP_EXCLUDE_NOTTY }
};

sgtk_enum_info sgtk_libgtop_process_selection_flags_type_info = {
    { "LibGTopProcessSelectionFlags", GTK_TYPE_FLAGS },
    3, _libgtop_process_selection_flags_literals,
};

void
glibtop_boot_guile_enums (void)
{
    sgtk_enum_flags_init (&sgtk_libgtop_parameter_type_info);
    sgtk_enum_flags_init (&sgtk_libgtop_method_type_info);
    sgtk_enum_flags_init (&sgtk_libgtop_error_method_type_info);
    sgtk_enum_flags_init (&sgtk_libgtop_error_type_info);
    sgtk_enum_flags_init (&sgtk_libgtop_process_selection_type_info);
    sgtk_enum_flags_init (&sgtk_libgtop_process_selection_flags_type_info);
}
