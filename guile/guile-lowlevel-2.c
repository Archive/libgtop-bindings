SCM_PROC (s_proclist, "glibtop-lowlevel-get-proclist", 4, 0, 0, glibtop_guile_lowlevel_get_proclist);

static SCM
glibtop_guile_lowlevel_get_proclist (SCM server_smob, SCM s_which,
				     SCM s_flags, SCM arg)
{
	glibtop_array array;
	glibtop *server;
	int64_t which, flags;
	unsigned * retval;
	SCM smob_array;
	int i;

	SCM_ASSERT ((SCM_FALSEP (server_smob) ||
		    (SCM_NIMP (server_smob)
		     && (SCM_CAR (server_smob) ==
			 scm_glibtop_smob_tags [GLIBTOP_STRUCTURE_GLIBTOP]))),
		    server_smob, SCM_ARG1, s_proclist);

	SCM_ASSERT (sgtk_valid_enum
		    (s_which,
		     &sgtk_libgtop_process_selection_type_info),
		    s_which, SCM_ARG2, s_proclist);

	SCM_ASSERT (sgtk_valid_flags
		    (s_flags,
		     &sgtk_libgtop_process_selection_flags_type_info),
		    s_flags, SCM_ARG3, s_proclist);

	server = SCM_FALSEP (server_smob) ? glibtop_global_server :
		(glibtop *) SCM_SMOB_DATA (server_smob);

	which = sgtk_scm2enum
		(s_which, &sgtk_libgtop_process_selection_type_info,
		 SCM_ARG2, (char *) s_proclist);

	flags = sgtk_scm2flags
		(s_flags, &sgtk_libgtop_process_selection_flags_type_info,
		 SCM_ARG3, (char *) s_proclist);

	retval = glibtop_get_proclist_l (server, &array, which | flags,
					 gh_scm2long (arg));

	if (retval == NULL)
		return SCM_BOOL_F;

	smob_array = scm_make_vector (SCM_MAKINUM (array.number), SCM_BOOL_F);
	for (i = 0; i < array.number; i++) {
		scm_vector_set_x (smob_array, SCM_MAKINUM (i),
				  gh_ulong2scm (retval [i]));
	}

	return smob_array;

}
