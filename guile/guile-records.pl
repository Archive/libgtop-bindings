#!/usr/bin/perl

require 'guile_types.pl';

die "Usage: $0 features.def structures.def" unless $#ARGV == 1;

$[ = 1;			# set array base to 1
$, = ' ';		# set output field separator
$\ = "\n";		# set output record separator

sub toupper {
    local($_) = @_;
    tr/a-z/A-Z/;
    return $_;
}

sub tolower {
    local($_) = @_;
    tr/A-Z/a-z/;
    return $_;
}

print '/* guile-records.c */';
print "/* This is a generated file.  Please modify `guile-records.pl' */";
print '';

print '#include "guile.h"';
print '#include <glibtop/structures.h>';
print '';

open FEATURESDEF, $ARGV[1] or
  die "open ($ARGV[1]): $!";

while (<FEATURESDEF>) {
  chop;				# strip record separator
  
  if (/^[^\#]/) {
    &parse_features_def ($_);
  }
}

close FEATURESDEF;

open STRUCTDEF, $ARGV[2] or
  die "open ($ARGV[2]): $!";

while (<STRUCTDEF>) {
  chop;				# strip record separator
  
  if (/^[^\#]/) {
    &parse_structure_def ($_);
  }
}

close STRUCTDEF;

sub parse_features_def {
  local($line) = @_;
  @line_fields = split(/\|/, $line, 9999);
  $retval = $line_fields[1];
  $element_def = $line_fields[3];
  $feature = $line_fields[2];
  $param_def = $line_fields[4];

  $feature =~ s/^@//;
  $name = 'glibtop_'.$feature;

  return if ($element_def eq 'array');

  $structures{$name} = $element_def;

  $is_from_structures_def{$name} = 0;
}

sub parse_structure_def {
  local($line) = @_;
  @line_fields = split(/\|/, $line, 9999);
  $name = $line_fields[1];
  $element_def = $line_fields[2];

  $structures{$name} = $element_def;

  $is_from_structures_def{$name} = 1;
}

$total_output = '';
$make_record_type_code = '';
$main_local_var_decl_code = '';

foreach $structure (keys %structures) {
  $structure_name = $structure;
  $structure_name =~ s/_/-/g;

  $field_list_code = '';
  $field_init_code = '';

  $field_number = 0;

  $element_def = $structures{$structure};
  $nr_elements = (@elements = split(/:/, $element_def, 9999));
  for ($element = 1; $element <= $nr_elements; $element++) {
    next if $elements[$element] eq 'array';

    $list = $elements[$element];
    $type = $elements[$element];
    $type =~ s/\(.*//;
    $list =~ s/^.*\(//;
    $list =~ s/\)$//;
    $count = (@fields = split(/,/, $list, 9999));

    if ($is_from_structures_def{$structure}) {
      if ($field_number == 0) {
	$have_flags_field = ($fields[0] eq 'flags');
      }
    } else {
      $have_flags_field = 1;
    }

    for ($field = 1; $field <= $count; $field++) {
      if ($fields[$field] =~ /^(\w+)\[([^\]]+)\]$/) {
	@field_parts = split(/\[/, $fields[$field], 9999);
	$fields[$field] = $field_parts[1];
	$field_parts[2] =~ s/\]//;
	
	$field_name = $field_parts[0];

	if ($have_flags_field) {
	  $field_init_code .= sprintf
	    (qq[\tif (buf->flags & (1L << %s_%s)) \{\n], toupper ($structure),
	     toupper ($field_name));

	  $field_init_code .= sprintf
	    (qq[\t\tSCM vector;\n\t\tint i;\n\n]);

	  $field_init_code .= sprintf
	    (qq[\t\tvector = scm_make_vector (SCM_MAKINUM (%s), SCM_BOOL_F);\n\n],
	     $field_parts[2]);

	  $field_init_code .= sprintf
	    (qq[\t\tfor (i = 0; i < %s; i++)\n], $field_parts[2]);

	  $field_init_code .= sprintf
	    (qq[\t\t\tscm_vector_set_x (vector, SCM_MAKINUM (i),\n\t\t\t\t\t  %s (buf->%s [i]));\n\n],
	     $typeinfo->{$type}->[1], $field_name);

	  $field_init_code .= sprintf
	    (qq[\t\tscm_struct_set_x (retval, SCM_MAKINUM (%d), vector);\n],
	     $field_number);

	  $field_init_code .= sprintf (qq[\t\}\n\n]);
	}
      } else {
	$field_name = $fields[$field];

	if ($have_flags_field and not ($fields[$field] eq 'flags')) {
	  $field_init_code .= sprintf
	    (qq[\tif (buf->flags & (1L << %s_%s))\n], toupper ($structure),
	     toupper ($field_name));
	}

	$field_init_code .= sprintf
	  (qq[\t\tscm_struct_set_x (retval, SCM_MAKINUM (%d),\n\t\t\t\t  %s (buf->%s));\n\n],
	   $field_number, $typeinfo->{$type}->[1], $field_name);
      }

      $field_number++;

      $field_list_code .= sprintf
	(qq[gh_symbol2scm ("%s"),\n\t\t ], $field_name);
    }
  }

  $constructor_proc_code = sprintf
    (qq[SCM_PROC (s_glibtop_record_constructor_%s, "glibtop-record-constructor-%s", %d, 0, 0, glibtop_guile_record_constructor_%s);],
     $structure, $structure_name, 1, $structure);

  $constructor_funcdecl_code = sprintf
    (qq[SCM\nglibtop_guile_record_constructor_%s (SCM smob)\n\{\n],
     $structure);

  $constructor_local_vars = sprintf
    (qq[\tSCM smob_type, retval = SCM_BOOL_F;\n\t%s *buf;\n], $structure);

  $constructor_return_code = sprintf
    (qq[\tbuf = (%s *) SCM_SMOB_DATA (smob);\n\n], $structure);

  $constructor_return_code .= sprintf
    (qq[\tretval = scm_make_struct (SCM_CDR (s_%s_type), SCM_MAKINUM (0), SCM_EOL);\n\n],
     $structure);

  $constructor_return_code .= sprintf
    (qq[%s\n], $field_init_code);

  $constructor_return_code .= sprintf
    (qq[\treturn retval;\n]);

  $constructor_check_args_code = sprintf
    (qq[\tsmob_type = scm_glibtop_smob_tags [GLIBTOP_STRUCTURE_%s];\n\n],
     &toupper($structure));

  $constructor_check_args_code .= sprintf
    (qq[\tSCM_ASSERT ((SCM_NIMP (smob) && (SCM_CAR (smob) == smob_type)),\n\t\t    smob, SCM_ARG1, s_glibtop_record_constructor_%s);\n\n], $structure);

  $constructor_code = sprintf
    (qq[%s\n\n%s%s\n%s%s\}\n], $constructor_proc_code,
     $constructor_funcdecl_code, $constructor_local_vars,
     $constructor_check_args_code,
     $constructor_return_code);

  $main_local_var_decl_code .= sprintf
    (qq[\tSCM s_%s_fields;\n], $structure);

  $scm_record_code = sprintf
    (qq[SCM_GLOBAL_VCELL (s_%s_type, "glibtop-record-%s");],
     $structure, $structure_name);

  $make_record_type_code .= sprintf
    (qq[\ts_%s_fields = gh_list\n\t\t(%sSCM_UNDEFINED);\n\n],
     $structure, $field_list_code);

  $make_record_type_code .= sprintf
    (qq[\tSCM_SETCDR (s_%s_type,\n\t\t    scm_apply (make_record_type,\n\t\t\t       SCM_CAR (s_%s_type),\n\t\t\t       SCM_LIST2 (s_%s_fields,\n\t\t\t\t\t  SCM_EOL)));\n\n], $structure, $structure, $structure);

  $total_output .= sprintf (qq[%s\n\n%s\n\n], $scm_record_code,
			    $constructor_code);
}

print $total_output;

print qq[void];
print qq[glibtop_boot_guile_records (void)];
print '{';
print qq[\tSCM make_record_type;];
print $main_local_var_decl_code;
print qq[#ifndef SCM_MAGIC_SNARFER];
print qq[#include "guile-records.x"];
print qq[#endif];
print '';
print qq[\tmake_record_type = SCM_CDR (scm_intern0 ("make-record-type"));\n];
print $make_record_type_code;
print '}';

