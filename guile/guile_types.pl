$typeinfo = {'long'	=> ['gh_long2scm',	'gh_scm2long'],
	     'ulong'	=> ['gh_ulong2scm',	'gh_scm2ulong'],
	     'pid_t'	=> ['gh_long2scm',	'gh_scm2long'],
	     'int'	=> ['gh_long2scm',	'gh_scm2long'],
	     'retval'	=> ['gh_long2scm',	'gh_scm2long'],
	     'ushort'	=> ['gh_long2scm',	'gh_scm2long'],
	     'unsigned'	=> ['gh_ulong2scm',	'gh_scm2ulong'],
	     'double'	=> ['gh_double2scm',	'gh_scm2double'],
	     'string'	=> ['gh_str02scm',	'gh_scm2newstr'],
	     };

1;
