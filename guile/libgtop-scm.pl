#!/usr/bin/perl

require 'guile_types.pl';

die "Usage: $0 libgtop.scm.in features.def structures.def" unless $#ARGV == 2;

$[ = 1;			# set array base to 1
$, = ' ';		# set output field separator
$\ = "\n";		# set output record separator

sub toupper {
    local($_) = @_;
    tr/a-z/A-Z/;
    return $_;
}

sub tolower {
    local($_) = @_;
    tr/A-Z/a-z/;
    return $_;
}

print ";;";
print ";; This is a generated file.";
print ";; Please modify `libgtop.scm.in' and `libgtop-scm.pl'";
print ";;";
print "";

open LIBGTOPSCM, $ARGV[1] or
  die "open ($ARGV[1]): $!";

while (<LIBGTOPSCM>) {
  chop; print;
}

close LIBGTOPSCM;

open FEATURESDEF, $ARGV[2] or
  die "open ($ARGV[2]): $!";

while (<FEATURESDEF>) {
  chop;				# strip record separator
  
  if (/^[^\#]/) {
    &parse_features_def ($_);
  }
}

close FEATURESDEF;

sub parse_features_def {
  local($line) = @_;
  @line_fields = split(/\|/, $line, 9999);
  $retval = $line_fields[1];
  $element_def = $line_fields[3];
  $feature = $line_fields[2];
  $param_def = $line_fields[4];

  $feature =~ s/^@//;

  if ($element_def =~ /^array/) {
    $array_functions{$feature} = $param_def;
  }

  if ($retval =~ /^(array|pointer)\(glibtop_(.*)\)$/) {
    $type = $2; $type =~ s/_/-/g;
    $retval_types{$feature} = $type;
  }

  $structures{$feature} = $param_def;
}

$total_output = '';
$func_def_code = '';

foreach $structure (keys %structures) {
  $structure_name = $structure;
  $structure_name =~ s/_/-/g;

  if (($structure eq 'proclist') || ($structure eq 'proc_args')) {
    next;
  }

  if (defined $array_functions{$structure}) {
    $retval_type = $retval_types{$structure};
    $func_def_code .= sprintf
      (qq[\(libgtop-make-func %s (%s . %s) server], $structure_name,
       $structure_name, (defined $retval_type) ? $retval_type : $structure_name);
  } else {
    $retval_type = $retval_types{$structure};
    $func_def_code .= sprintf
      (qq[\(libgtop-make-func %s %s server], $structure_name,
       (defined $retval_type) ? $retval_type : $structure_name);
  }

  $element_def = $structures{$structure};
  $nr_elements = (@elements = split(/:/, $element_def, 9999));
  for ($element = 1; $element <= $nr_elements; $element++) {
    $list = $elements[$element];
    $type = $elements[$element];
    $type =~ s/\(.*//;
    $list =~ s/^.*\(//;
    $list =~ s/\)$//;
    $count = (@fields = split(/,/, $list, 9999));

    for ($field = 1; $field <= $count; $field++) {
      if ($fields[$field] =~ /^(\w+)\[([^\]]+)\]$/) {
	@field_parts = split(/\[/, $fields[$field], 9999);
	$fields[$field] = $field_parts[1];
	$field_parts[2] =~ s/\]//;
	
	$field_name = $field_parts[0];
      } else {
	$field_name = $fields[$field];
      }
      $func_def_code .= ' '.$field_name;
    }
  }

  $func_def_code .= ")\n";
}

print "";
print ";;";
print ";; (glibtop-get-*) functions.";
print ";;";
print "";
print $func_def_code;
