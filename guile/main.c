/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 4 -*- */

/* $Id$ */

/* Copyright (C) 1998-99 Martin Baulig
   This file is part of LibGTop 1.0.

   Contributed by Martin Baulig <martin@home-of-linux.org>, April 1998.

   LibGTop is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

   LibGTop is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
   for more details.

   You should have received a copy of the GNU General Public License
   along with LibGTop; see the file COPYING. If not, write to the
   Free Software Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.
*/

#include "guile.h"

long scm_glibtop_smob_tags [GLIBTOP_MAX_STRUCTURES];
SCM scm_glibtop_global_server_smob;

SCM_GLOBAL_VCELL (s_glibtop_global_server, "glibtop-global-server");

void
scm_debug_print (SCM obj)
{
    scm_display (obj, scm_current_output_port ());
    scm_newline (scm_current_output_port ());
    scm_flush (scm_current_output_port ());
}

void
scm_libgtop_guile_error (glibtop *server, int error)
{
    char *message;
    SCM tag;

    message = glibtop_get_error_string_l (server, -error);
    if (!message) {
	error = -GLIBTOP_ERROR_UNKNOWN;
	message = glibtop_get_error_string_l (server, error);
    }

    tag = sgtk_enum2scm (-error, &sgtk_libgtop_error_type_info);

    scm_throw (tag, SCM_LIST1 (scm_makfrom0str (message)));
}

SCM_PROC (s_glibtop_open, "glibtop-open", 1, 0, 0, scm_glibtop_open);

SCM
scm_glibtop_open (SCM server_smob)
{
    glibtop *server;

    SCM_ASSERT ((SCM_FALSEP (server_smob) ||
		 (SCM_NIMP (server_smob)
		  && (SCM_CAR (server_smob) ==
		      scm_glibtop_smob_tags [GLIBTOP_STRUCTURE_GLIBTOP]))),
		server_smob, SCM_ARG1, s_glibtop_open);

    server = SCM_FALSEP (server_smob) ? glibtop_global_server :
	(glibtop *) SCM_SMOB_DATA (server_smob);

    glibtop_init_r (&server, 0, 0);

    return SCM_UNSPECIFIED;
}

SCM_PROC (s_glibtop_init, "glibtop-init", 0, 0, 0, scm_glibtop_init);

SCM
scm_glibtop_init (void)
{
    SCM server_smob;
    glibtop *server;

    server_smob = scm_make_smob
	(scm_glibtop_smob_tags [GLIBTOP_STRUCTURE_GLIBTOP]);
    server = (glibtop *) SCM_SMOB_DATA (server_smob);

    glibtop_server_ref (server);

    glibtop_init_r (&server, 0, GLIBTOP_INIT_NO_OPEN);

    return server_smob;
}

SCM_PROC (s_glibtop_get_parameter, "glibtop-get-parameter", 2, 0, 0, scm_glibtop_get_parameter);

SCM
scm_glibtop_get_parameter (SCM s_server, SCM s_parameter)
{
    glibtop *server;
    gint parameter;

    SCM_ASSERT ((SCM_FALSEP (s_server) ||
		 (SCM_NIMP (s_server)
		  && (SCM_CAR (s_server) ==
		      scm_glibtop_smob_tags [GLIBTOP_STRUCTURE_GLIBTOP]))),
		s_server, SCM_ARG1, s_glibtop_get_parameter);

    SCM_ASSERT (sgtk_valid_enum (s_parameter, &sgtk_libgtop_parameter_type_info),
		s_parameter, SCM_ARG2, s_glibtop_get_parameter);

    server = SCM_FALSEP (s_server) ? glibtop_global_server :
	(glibtop *) SCM_SMOB_DATA (s_server);

    parameter = sgtk_scm2enum (s_parameter, &sgtk_libgtop_parameter_type_info,
			       SCM_ARG2, (char *) s_glibtop_get_parameter);

    switch (parameter) {
    case GLIBTOP_PARAM_ERROR_METHOD:
	do {
	    unsigned error_method;
	    int retval;

	    retval = glibtop_get_parameter_l
		(server, parameter, &error_method, sizeof (error_method));

	    if (retval < 0) {
		scm_libgtop_guile_error (server, retval);
		g_assert_not_reached ();
	    }

	    return sgtk_enum2scm (error_method,
				  &sgtk_libgtop_error_method_type_info);
	} while (0);
	g_assert_not_reached ();
    case GLIBTOP_PARAM_FEATURES:
	do {
	    u_int64_t features;
	    int retval;

	    retval = glibtop_get_parameter_l
		(server, parameter, &features, sizeof (features));

	    if (retval < 0) {
		scm_libgtop_guile_error (server, retval);
		g_assert_not_reached ();
	    }

	    return SCM_MAKINUM (features);
	} while (0);
	g_assert_not_reached ();
    case GLIBTOP_PARAM_NCPU:
	do {
	    unsigned ncpu;
	    int retval;

	    retval = glibtop_get_parameter_l
		(server, parameter, &ncpu, sizeof (ncpu));

	    if (retval < 0) {
		scm_libgtop_guile_error (server, retval);
		g_assert_not_reached ();
	    }

	    return SCM_MAKINUM (ncpu);
	} while (0);
	g_assert_not_reached ();
    case GLIBTOP_PARAM_OS_VERSION_CODE:
	do {
	    u_int64_t os_version_code;
	    int retval;

	    retval = glibtop_get_parameter_l
		(server, parameter, &os_version_code, sizeof (os_version_code));

	    if (retval < 0) {
		scm_libgtop_guile_error (server, retval);
		g_assert_not_reached ();
	    }

	    return SCM_MAKINUM (os_version_code);
	} while (0);
	g_assert_not_reached ();
    case GLIBTOP_PARAM_REQUIRED:
	do {
	    glibtop_sysdeps required;
	    SCM smob_answer;
	    int retval;

	    retval = glibtop_get_parameter_l
		(server, parameter, &required, sizeof (required));

	    if (retval < 0)
		scm_libgtop_guile_error (server, retval);

	    smob_answer = scm_make_smob
		(scm_glibtop_smob_tags [GLIBTOP_STRUCTURE_GLIBTOP_SYSDEPS]);
	    *(glibtop_sysdeps *) SCM_SMOB_DATA (smob_answer) = required;

	    return smob_answer;
	} while (0);
	g_assert_not_reached ();
    }

    scm_libgtop_guile_error (server, -GLIBTOP_ERROR_NO_SUCH_PARAMETER);
    g_assert_not_reached ();
}

SCM_PROC (s_glibtop_set_parameter, "glibtop-set-parameter", 3, 0, 0, scm_glibtop_set_parameter);

SCM
scm_glibtop_set_parameter (SCM s_server, SCM s_parameter, SCM s_value)
{
    glibtop *server;
    gint parameter;

    SCM_ASSERT ((SCM_FALSEP (s_server) ||
		 (SCM_NIMP (s_server)
		  && (SCM_CAR (s_server) ==
		      scm_glibtop_smob_tags [GLIBTOP_STRUCTURE_GLIBTOP]))),
		s_server, SCM_ARG1, s_glibtop_set_parameter);

    SCM_ASSERT (sgtk_valid_enum (s_parameter, &sgtk_libgtop_parameter_type_info),
		s_parameter, SCM_ARG2, s_glibtop_set_parameter);

    server = SCM_FALSEP (s_server) ? glibtop_global_server :
	(glibtop *) SCM_SMOB_DATA (s_server);

    parameter = sgtk_scm2enum (s_parameter, &sgtk_libgtop_parameter_type_info,
			       SCM_ARG2, (char *) s_glibtop_set_parameter);

    switch (parameter) {
    case GLIBTOP_PARAM_ERROR_METHOD:
	do {
	    unsigned error_method;
	    int retval;

	    SCM_ASSERT (sgtk_valid_enum (s_value,
					 &sgtk_libgtop_error_method_type_info),
			s_value, SCM_ARG3, s_glibtop_set_parameter);

	    error_method = sgtk_scm2enum
		(s_value, &sgtk_libgtop_error_method_type_info,
		 SCM_ARG3, (char *) s_glibtop_set_parameter);

	    retval = glibtop_set_parameter_l
		(server, parameter, &error_method, sizeof (error_method));

	    if (retval < 0) {
		scm_libgtop_guile_error (server, retval);
		g_assert_not_reached ();
	    }

	    return SCM_UNSPECIFIED;
	} while (0);
	g_assert_not_reached ();
    case GLIBTOP_PARAM_FEATURES:
    case GLIBTOP_PARAM_NCPU:
    case GLIBTOP_PARAM_OS_VERSION_CODE:
	scm_libgtop_guile_error (server, -GLIBTOP_ERROR_READONLY_VALUE);
	g_assert_not_reached ();
    case GLIBTOP_PARAM_REQUIRED:
	do {
	    glibtop_sysdeps required;
	    SCM smob_answer;
	    int retval;
	    
	    SCM_ASSERT (SCM_NIMP (s_value) &&
			(SCM_CAR (s_value) ==
			 scm_glibtop_smob_tags [GLIBTOP_STRUCTURE_GLIBTOP_SYSDEPS]),
			s_value, SCM_ARG1, s_glibtop_set_parameter);

	    required = *(glibtop_sysdeps *) SCM_SMOB_DATA (s_value);

	    retval = glibtop_set_parameter_l
		(server, parameter, &required, sizeof (required));

	    if (retval < 0) {
		scm_libgtop_guile_error (server, retval);
		g_assert_not_reached ();
	    }

	    return SCM_UNSPECIFIED;
	} while (0);
	g_assert_not_reached ();
    }

    scm_libgtop_guile_error (server, -GLIBTOP_ERROR_NO_SUCH_PARAMETER);
    g_assert_not_reached ();
}

SCM_PROC (s_glibtop_open_backend, "glibtop-open-backend", 2, 0, 0, scm_glibtop_open_backend);

SCM
scm_glibtop_open_backend (SCM s_server, SCM s_name)
{
    glibtop *server;
    int retval;

    SCM_ASSERT ((SCM_FALSEP (s_server) ||
		 (SCM_NIMP (s_server)
		  && (SCM_CAR (s_server) ==
		      scm_glibtop_smob_tags [GLIBTOP_STRUCTURE_GLIBTOP]))),
		s_server, SCM_ARG1, s_glibtop_open_backend);

    SCM_ASSERT (SCM_NIMP (s_name) && SCM_STRINGP (s_name),
		s_name, SCM_ARG2, s_glibtop_open_backend);

    server = SCM_FALSEP (s_server) ? glibtop_global_server :
	(glibtop *) SCM_SMOB_DATA (s_server);

    retval = glibtop_open_backend_l (server, SCM_CHARS (s_name),
				     0, NULL);

    return SCM_MAKINUM (retval);
}

SCM_PROC (s_glibtop_close, "glibtop-close", 1, 0, 0, scm_glibtop_close);

SCM
scm_glibtop_close (SCM s_server)
{
    glibtop *server;

    SCM_ASSERT ((SCM_FALSEP (s_server) ||
		 (SCM_NIMP (s_server)
		  && (SCM_CAR (s_server) ==
		      scm_glibtop_smob_tags [GLIBTOP_STRUCTURE_GLIBTOP]))),
		s_server, SCM_ARG1, s_glibtop_close);

    server = SCM_FALSEP (s_server) ? glibtop_global_server :
	(glibtop *) SCM_SMOB_DATA (s_server);

    glibtop_close_r (server);

    return SCM_UNSPECIFIED;
}

void
glibtop_boot_guile (void)
{
#ifndef SCM_MAGIC_SNARFER
#include "main.x"
#endif

    glibtop_boot_guile_lowlevel ();
    glibtop_boot_guile_records ();
    glibtop_boot_guile_enums ();

    SCM_NEWSMOB (scm_glibtop_global_server_smob,
		 scm_glibtop_smob_tags [GLIBTOP_STRUCTURE_GLIBTOP],
		 glibtop_global_server);

    glibtop_server_ref (glibtop_global_server);

    glibtop_init_r (&glibtop_global_server, 0, GLIBTOP_INIT_NO_OPEN);

    glibtop_init_backends ();

    SCM_SETCDR (s_glibtop_global_server, scm_glibtop_global_server_smob);
}
