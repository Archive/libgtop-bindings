(define-module (libgtop names)
  :use-module (gtk dynlink)
  :use-module (libgtop libgtop))

(merge-compiled-code "scm_init_libgtop_names_module" "libgtop_guile_names_dynl")

