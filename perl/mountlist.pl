#!/usr/bin/perl -w

require 5.004;

use blib;
use strict;
use Libgtop;

my $server = Libgtop->new;

sub output($@) {
  my ($name, @data) = @_;

  my $output = '';
  my $label = sprintf ("%-12s (%08lx):", $name, shift @data);

  foreach my $data (@data) {
    if (ref $data) {
      $output .= '( '.join (' ', @$data).' )';
    } else {
      $output .= $data;
    }
    $output .= ' ';
  }

  printf ("%s  %s\n", $label, $output);

}

print "\n";

my @mountlist = $server->mountlist (1);
my $entries = pop @mountlist;

output ("mountlist", @mountlist);

print "\n";

foreach my $entry (@$entries) {
  printf +("%-20s %-30s %-10s %d\n",
	   $entry->[1], $entry->[2], $entry->[3], $entry->[0]);
}

print "\n\n";
printf +("%-20s %9s %9s %9s %9s %9s\n", "", "Blocks", "Free",
	 "Avail", "Files", "Free");
print "\n";

foreach my $entry (@$entries) {
  my @fsusage = $server->fsusage ($entry->[2]);

  shift @fsusage;
  printf +("%-20s %9ld %9ld %9ld %9ld %9ld\n",
	   $entry->[2], @fsusage);
}

print "\n";
