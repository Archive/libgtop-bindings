#!/usr/bin/perl -w

require 5.004;

use blib;
use strict;
use Libgtop;
use Socket;

if ($#ARGV != 0) {
  print STDERR "Usage: $0 interface\n";
  exit 1;
}

my $server = Libgtop->new;

sub output($@) {
  my ($name, @data) = @_;

  my $output = '';
  my $label = sprintf ("%-12s (%08lx):", $name, shift @data);

  foreach my $data (@data) {
    if (ref $data) {
      $output .= '( '.join (' ', @$data).' )';
    } else {
      $output .= $data;
    }
    $output .= ' ';
  }

  printf ("%s  %s\n", $label, $output);

}

my @netload = $server->netload ($ARGV[0]);

printf "\nNetwork Load (0x%08lx)\n\n", shift @netload;

my $address = inet_ntoa (pack "c4", unpack ("c4", pack ("L", $netload [3])));
my $subnet  = inet_ntoa (pack "c4", unpack ("c4", pack ("L", $netload [2])));

printf "\t%-24s0x%08lx\n", "Interface Flags:", $netload [0];
printf "\t%-24s0x%08lx - %s\n", "Address:", $netload [3], $address;
printf "\t%-24s0x%08lx - %s\n\n", "Subnet:",  $netload [2], $subnet;
printf "\t%-24s%ld\n", "MTU:", $netload [1];
printf "\t%-24s%ld\n\n", "Collisions:",  $netload [13];
printf "\t%-24s%ld\n", "Packets In:", $netload [4];
printf "\t%-24s%ld\n", "Packets Out:", $netload [5];
printf "\t%-24s%ld\n\n", "Packets Total:", $netload [6];
printf "\t%-24s%ld\n", "Bytes In:", $netload [7];
printf "\t%-24s%ld\n", "Bytes Out:", $netload [8];
printf "\t%-24s%ld\n\n", "Bytes Total:", $netload [9];
printf "\t%-24s%ld\n", "Errors In:", $netload [10];
printf "\t%-24s%ld\n", "Errors Out:", $netload [11];
printf "\t%-24s%ld\n\n", "Errors Total:", $netload [12];
