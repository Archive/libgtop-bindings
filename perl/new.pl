#!/usr/bin/perl -w

require 5.004;

use blib;
use strict;
use Libgtop;

my $server = Libgtop->new;

sub output($@) {
  my ($name, @data) = @_;

  my $output = '';
  my $label = sprintf ("%-12s (%08lx):", $name, shift @data);

  foreach my $data (@data) {
    if (ref $data) {
      $output .= '( '.join (' ', @$data).' )';
    } else {
      $output .= $data;
    }
    $output .= ' ';
  }

  printf ("%s  %s\n", $label, $output);

}

output ("cpu", $server->cpu);
output ("mem", $server->mem);
output ("swap", $server->swap);

print "\n";

output ("uptime", $server->uptime);
output ("loadavg", $server->loadavg);

print "\n";

output ("shm_limits", $server->shm_limits);
output ("msg_limits", $server->msg_limits);
output ("sem_limits", $server->sem_limits);

print "\n";

output ("ppp", $server->ppp (0));

print "\n";

output ("proclist", $server->proclist (0,0));

print "\n";

output ("proc_state", $server->proc_state ($$));
output ("proc_uid", $server->proc_uid ($$));
output ("proc_mem", $server->proc_mem ($$));
output ("proc_time", $server->proc_time ($$));
output ("proc_signal", $server->proc_signal ($$));
output ("proc_kernel", $server->proc_kernel ($$));
output ("proc_segment", $server->proc_segment ($$));

print "\n";

output ("proc_args", $server->proc_args ($$,0));

print "\n";

