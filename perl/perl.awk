BEGIN {
  print "/* Libgtop.xs */";
  print "/* This is a generated file.  Please modify `perl.awk' */";
  print "";

  print "#ifdef __cplusplus";
  print "extern \"C\" {";
  print "#endif";
  print "#include \"EXTERN.h\"";
  print "#include \"perl.h\"";
  print "#include \"XSUB.h\"";
  print "#ifdef __cplusplus";
  print "}";
  print "#endif";
  print "";
  print "#undef PACKAGE";
  print "";
  print "#include <glibtop.h>";
  print "#include <glibtop/union.h>";
  print "#include <glibtop/xmalloc.h>";
  print "";
  print "MODULE = Libgtop\t\tPACKAGE = Libgtop";
  print "";
  print "PROTOTYPES: ENABLE";
  print "";
  print "glibtop *";
  print "init (CLASS)";
  print "\tchar *CLASS;";
  print "CODE:";
  print "\tRETVAL = glibtop_calloc (1, sizeof (glibtop));";
  print "\tglibtop_init_s (&RETVAL, 0, 0);";
  print "OUTPUT:";
  print "\tRETVAL";
  print "";

  type_convert["long"]     = "int64_t";
  type_convert["ulong"]    = "u_int64_t";
  type_convert["pid_t"]    = "pid_t";
  type_convert["int"]      = "int";
  type_convert["unsigned"] = "unsigned";
  type_convert["ushort"]   = "ushort";

  convert["long"]   = "newSViv";
  convert["ulong"]  = "newSViv";
  convert["double"] = "newSVnv";
}

function output(line) {
  split (line, line_fields, /\|/);
  retval = line_fields[1];
  feature = line_fields[2];
  element_def = line_fields[3];
  param_def = line_fields[4];

  orig = feature; sub(/^@/,"",feature);
  space = feature; gsub(/./," ",space);

  if (param_def == "string") {
    call_param = ", "line_fields[5];
    param_decl = "\tconst char *"line_fields[5]";\n";
  } else {
    call_param = "";
    param_decl = "";
    nr_params = split (param_def, params, /:/);
    for (param = 1; param <= nr_params; param++) {
      list = params[param];
      type = params[param];
      sub(/\(.*/, "", type);
      sub(/^.*\(/, "", list); sub(/\)$/, "", list);
      count = split (list, fields, /,/);
      for (field = 1; field <= count; field++) {
	param_decl = param_decl"\t";
	call_param = call_param", ";
	param_decl = param_decl""type_convert[type]" "fields[field]";\n";
	call_param = call_param""fields[field];
      }
    }
  }
  
  print "void";
  print feature" (server"call_param")";
  print "\tglibtop *server;";
  print param_decl"PREINIT:";
  print "\tglibtop_"feature" "feature";";
  print "\tAV *temp;";

  if (feature ~ /^proclist/) {
    print "\tunsigned i, *ptr;";
    print "PPCODE:";
    print "\tptr = glibtop_get_proclist_l (server, &proclist, which, arg);";
  } else if (feature ~ /^proc_args/) {
    print "\tunsigned i;";
    print "\tchar *retval, *start;";
    print "PPCODE:";
    print "\tretval = glibtop_get_proc_args_l";
    print "\t\t(server, &proc_args, pid, max_len);";
  } else if (feature ~ /^mountlist$/) {
    print "\tunsigned i;";
    print "\tglibtop_mountentry *retval;";
    print "PPCODE:";
    print "\tretval = glibtop_get_mountlist_l (server, &mountlist, all_fs);";
  } else if (feature ~ /^proc_map$/) {
    print "\tunsigned i;";
    print "\tglibtop_map_entry *retval;";
    print "PPCODE:";
    print "\tretval = glibtop_get_proc_map_l (server, &proc_map, pid);";
  } else {
    print "PPCODE:";
    if (call_param != "")
      print "\tglibtop_get_"feature"_l (server, &"feature""call_param");";
    else
      print "\tglibtop_get_"feature"_l (server, &"feature");";
  }
  
  print "";
  
  print "\tXPUSHs (sv_2mortal (newSViv ("feature".flags)));\n";

  total_number = 1;
  
  nr_elements = split (element_def, elements, /:/);
  for (element = 1; element <= nr_elements; element++) {
    list = elements[element];
    type = elements[element];
    sub(/\(.*/, "", type);
    sub(/^.*\(/, "", list); sub(/\)$/, "", list);
    count = split (list, fields, /,/);
    for (field = 1; field <= count; field++) {
      if (fields[field] ~ /^(\w+)\[([0-9]+)\]$/) {
	split(fields[field], field_parts, /\[/);
	fields[field] = field_parts[1];
	sub(/\]/, "", field_parts[2]);
	number = field_parts[2];
	print "\ttemp = newAV ();";
	for (nr = 0; nr < number; nr++) {
	  total_number++;
	  if (type ~ /^str$/) {
	    print "\tav_push (temp, sv_2mortal (newSVpv ("feature"."fields[field]" ["nr"], 0)));";
	  } else {
	    if (type ~ /^char$/) {
	      print "\tav_push (temp, sv_2mortal (newSVpv (&"feature"."fields[field]" ["nr"], 1)));";
	    } else {
	      print "\tav_push (temp, sv_2mortal ("convert[type]" ("feature"."fields[field]" ["nr"])));";
	    }
	  }
	}
	print "\tXPUSHs (sv_2mortal (newRV_inc ((SV*) temp)));";
      } else {
	total_number++;
	if (type ~ /^str$/) {
	  print "\tXPUSHs (sv_2mortal (newSVpv ("feature"."fields[field]", 0)));";
	} else {
	  if (type ~ /^char$/) {
	    print "\tXPUSHs (sv_2mortal (newSVpv (&"feature"."fields[field]", 1)));";
	  } else {
	    print "\tXPUSHs (sv_2mortal ("convert[type]" ("feature"."fields[field]")));";
	  }
	}
      }
    }
  }

  print "";

  if (feature ~ /^proclist/) {
    print "\tif (ptr == NULL)";
    print "\t\tXSRETURN ("total_number");";
    print "";
    print "\ttemp = newAV ();";
    print "\tfor (i = 0; i < proclist.number; i++)";
    print "\t\tav_push (temp, sv_2mortal (newSViv (ptr [i])));";
    print "\tXPUSHs (sv_2mortal (newRV_inc ((SV*) temp)));";
    print "";
    print "\tglibtop_free (ptr);";
    print "";
  }

  if (feature ~ /^proc_args/) {
    print "\tif (retval == NULL)";
    print "\t\tXSRETURN ("total_number");";
    print "";
    print "\tstart = retval;";
    print "\ttemp = newAV ();";
    print "";
    print "\tfor (i = 0; i <= proc_args.size; i++) {";
    print "\t\tif (retval [i]) continue;";
    print "\t\tav_push (temp, sv_2mortal (newSVpv (start, 0)));";
    print "\t\tstart = &(retval [i+1]);";
    print "\t}";
    print "";
    print "\tXPUSHs (sv_2mortal (newRV_inc ((SV*) temp)));";
    print "";
    print "\tglibtop_free (retval);";
    print "";
  }

  if (feature ~ /^mountlist/) {
    print "\tif (retval == NULL)";
    print "\t\tXSRETURN ("total_number");";
    print "";
    print "\ttemp = newAV ();";
    print "";
    print "\tfor (i = 0; i < mountlist.number; i++) {";
    print "\t\tglibtop_mountentry *entry = &(retval [i]);";
    print "\t\tAV *entry_av = newAV ();";
    print "";
    print "\t\tav_push (entry_av, newSViv (entry->dev));";
    print "\t\tav_push (entry_av, newSVpv (entry->devname, 0));";
    print "\t\tav_push (entry_av, newSVpv (entry->mountdir, 0));";
    print "\t\tav_push (entry_av, newSVpv (entry->type, 0));";
    print "";
    print "\t\tav_push (temp, newRV_inc ((SV*) entry_av));";
    print "\t}";
    print "";
    print "\tXPUSHs (sv_2mortal (newRV_inc ((SV*) temp)));";
    print "\tglibtop_free (retval);";
    print "";
  }

  if (feature ~ /^proc_map/) {
    print "\tif (retval == NULL)";
    print "\t\tXSRETURN ("total_number");";
    print "";
    print "\ttemp = newAV ();";
    print "";
    print "\tfor (i = 0; i < proc_map.number; i++) {";
    print "\t\tglibtop_map_entry *entry = &(retval [i]);";
    print "\t\tAV *entry_av = newAV ();";
    print "";
    print "\t\tav_push (entry_av, newSViv (entry->flags));";
    print "\t\tav_push (entry_av, newSViv (entry->start));";
    print "\t\tav_push (entry_av, newSViv (entry->end));";
    print "\t\tav_push (entry_av, newSViv (entry->offset));";
    print "\t\tav_push (entry_av, newSViv (entry->perm));";
    print "\t\tav_push (entry_av, newSViv (entry->inode));";
    print "\t\tav_push (entry_av, newSViv (entry->device));";
    print "\t\tav_push (entry_av, newSVpv (entry->filename, 0));";
    print "";
    print "\t\tav_push (temp, newRV_inc ((SV*) entry_av));";
    print "\t}";
    print "";
    print "\tXPUSHs (sv_2mortal (newRV_inc ((SV*) temp)));";
    print "\tglibtop_free (retval);";
    print "";
  }
    
  print "";
}

/^[^#]/		{ output($0) }

