#!/usr/bin/perl -w

require 5.004;

use blib;
use strict;
use Libgtop;

if ($#ARGV != 0) {
  print STDERR "Usage: $0 pid\n";
  exit 1;
}

my $server = Libgtop->new;

sub output($@) {
  my ($name, @data) = @_;

  my $output = '';
  my $label = sprintf ("%-12s (%08lx):", $name, shift @data);

  foreach my $data (@data) {
    if (ref $data) {
      $output .= '( '.join (' ', @$data).' )';
    } else {
      $output .= $data;
    }
    $output .= ' ';
  }

  printf ("%s  %s\n", $label, $output);

}

print "\n";

my @proc_map = $server->proc_map ($ARGV[0]);
my $entries = pop @proc_map;

output ("procmap", @proc_map);

print "\n";

foreach my $entry (@$entries) {
  my ($flags,$start,$end,$offset,$perm,$inode,$device,$filename) = @$entry;

  my $perm_string = '';
  $perm_string .= ($perm &  1) ? 'r' : '-';
  $perm_string .= ($perm &  2) ? 'w' : '-';
  $perm_string .= ($perm &  4) ? 'x' : '-';
  $perm_string .= ($perm &  8) ? 's' : '-';
  $perm_string .= ($perm & 16) ? 'p' : '-';

  my $dev_major = (($device >> 8) & 255);
  my $dev_minor = ($device & 255);

  printf +("%08x-%08x %08x - %02x:%02x %08lx - %s - %s\n", $start, $end,
	   $offset, $dev_major, $dev_minor, $inode, $perm_string, $filename);
}

print "\n";
